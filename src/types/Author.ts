import type Book from "./Books";

export default interface Author {
  author_id: number;
  author_name: string;
  books: Book[];
  }