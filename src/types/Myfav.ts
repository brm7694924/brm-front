// Myfav.ts
import type User from "./User";

export default interface Myfav {
    myfav_id?: number;
    user_id?: User;
    book?: Array<{
        book_id: number;
        book_title: string;
        book_content: string;
        book_score: number;
        book_pros?: string;
        book_cons?: string;
        book_img: string;
        createdAt: string;
        updatedAt: string;
        deletedAt?: string;
    }>;
}
