import type Review from "./Review";

export default interface ReportReview {
    rep_rev_id: number;
    review: Review;
    }