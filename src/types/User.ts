import type Role from "./Role";
import type Myfavorite from "./Myfav";
import type FeedComment from "./FeedCom";
export default interface User {
  user_id?: number;
  user_email?: string;
  user_name?: string;
  user_password?: string;
  user_img?: string;
  files?: File[];
  user_start_date?: Date;
  role?: Role;
  feedComment?: FeedComment[];
  myfavorite?: Myfavorite[];
}