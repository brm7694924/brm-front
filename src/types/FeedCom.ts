import type Feed from "./Feed";
import type User from "./User";

export default interface FeedComment {
  feed_com_id: number;
  feed_com_text: string;
  feed: Feed;
  user: User;
  }