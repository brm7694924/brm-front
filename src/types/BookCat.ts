import type Books from "./Books";
import type Category from "./Category";

export default interface BookCat {
    bookcat_id?: Number;
    book_id? :  Books ;    
    category_id? : Category;
  }