export default interface Report {
    rep_id: number;
    rep_name: string;
    rep_user: string;
  }