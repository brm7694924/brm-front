import type Book from "./Books";

export default interface Category {
    category_id?: Number;
    category_name?: String;
    books: Book[];
  }