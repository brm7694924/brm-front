import type Feed from "./Feed";

export default interface ReportFeed {
    rep_feed_id: number;
    feed: Feed;
    }