import type User from "./User";

export default interface Role {
    role_id: number;
    role_name: string;
    user: User[];
}