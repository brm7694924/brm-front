import type Author from "./Author";
import type Books from "./Books";

export default interface BookAut {
  book_author_id: number;
  book_id?: Books;
  author_id?: Author;
}