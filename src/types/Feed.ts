import type FeedComment from "./FeedCom";
import type ReportFeed from "./reportFeed";
import type User from "./User";
export default interface FeedCom {
  feed_id: number; // คุณสมบัตินี้จะต้องมีอยู่ใน FeedCom
  feed_img: string;
  feed_title: string;
  feed_text: string;
  feed_like?: number;
  feed_dislike?: number;
  feed_date: Date;
  feedComment: FeedComment[]; // อาร์เรย์ของ FeedComment
  reportFeed: ReportFeed[]; // อาร์เรย์ของ ReportFeed
  user?: User[];
}