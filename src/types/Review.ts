import type Books from "./Books";
import type ReportReview from "./reportRev";
import type User from "./User";
export default interface Review {
    rev_id?: number;
    rev_star?: number;
    rev_comment?: string;
    rev_date?: Date;
    book?: Books; 
    user?: User[];
    reportReview?: ReportReview[];
  }