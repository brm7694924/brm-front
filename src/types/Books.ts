import type Author from "./Author";
import type Category from "./Category";
import type Myfav from "./Myfav";


export default interface Book {
push(arg0: (number | undefined)[]): unknown;
    book_id?: number;
    book_title?: string;
    book_content?: string;
    book_score?: number;
    book_pros?: string;
    book_cons?: string;
    book_img?: string;
    myfav_id?: Myfav[];
    categories?: Category[];
    authors?: Author[];
    createdAt? : Date;
    updatedAt? : Date;
    deletedAt?: Date;
  }