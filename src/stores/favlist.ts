import { ref } from 'vue'
import { defineStore } from 'pinia'
import favlistService from '@/services/favlist'
import type Myfav from '@/types/Myfav'
import type Book from '@/types/Books'
export const usefavlistStore = defineStore('favlist', () => {
  const favBook = ref<Book[]>([])

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const isLoading = ref(false)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const errorMessage = ref('')
  const listMyBook = ref<Book[]>([])

  
  async function getFavlist() {
    try {
      const res = await favlistService.getFavlist()
      favBook.value = res.data
      console.log(favBook.value)
    } catch (e) {
      console.log(e)
    }
  }
  function addBookToFavList(book: any) {
    const existingBookIndex = favBook.value.findIndex((item) => item.myfav_id === book.id)

    if (existingBookIndex === -1) {
      favBook.value.push(book)
      console.log(`${book.title}`)
    } else {
      console.log(`${book.title}`)
    }
  }
  

  
  const bookByUser = ref<Myfav[]>([])
  async function getFavlistByUserId(id: number) {
    // ดึงข้อมูลจาก service
    const resData = await favlistService.getFavlist()

    // เก็บข้อมูลใน favBook
    favBook.value = resData.data

    // คัดกรองเฉพาะหนังสือที่ตรงกับ user_id ที่ระบุ
    bookByUser.value = favBook.value
      .filter((item) => item.user.user_id === id) // คัดกรองตาม user_id
      .flatMap((item) => item.book || []) // ดึงข้อมูลเฉพาะของหนังสือ

    // แสดงผลข้อมูลที่ดึงได้
    console.log(bookByUser.value)
  }

  async function addNewMyfav(userId: number, bookId: number) {
    const resService = await favlistService.addNewMyfavoritesBook(userId, bookId)
    console.log(resService.data)
  }
  return {
    getFavlist,
    favBook,
    addBookToFavList,
    getFavlistByUserId,
    bookByUser,
    listMyBook,
    addNewMyfav
  }
})
