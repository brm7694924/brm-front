import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Books from '@/types/Books';
import bookService from '@/services/books';

export const useBooksStore = defineStore('books', () => {
  const books = ref<Books[]>([]);
  const booksAllAll = ref<Books[]>([]);
  const book = ref<Books | null>(null);
  const bookAll = ref<Books | null>(null);
  const editedBook = ref<Books>({
    book_id: 1,
    book_title: "",
    book_content: "",
    book_score: 0,
    book_pros: "",
    book_cons: "",
    book_img: ""
  });
  const ListBook = ref<Books>({
    book_id: 0,
    book_title: "",
    book_content: "",
    book_score: 0,
    book_pros: "",
    book_cons: "",
    book_img: ""
  });
  const ListBookAll = ref<Books>({
    book_id: 0,
    book_title: "",
    book_content: "",
    book_score: 0,
    book_pros: "",
    book_cons: "",
    book_img: ""
  });
  const bookByCate = ref<Books[]>([]);
  const isLoading = ref(false);
  const errorMessage = ref(""); 
  const keepBookId = ref<number>()
  async function getBooks() {
    isLoading.value = true; // Start loading
    try {
      const res = await bookService.getBooks();
      books.value = res.data;

      if (books.value.length > 0) {
        book.value = books.value[0];
        ListBook.value = books.value[0];
      }
    } catch (e) {
      errorMessage.value = "Failed to load books.";
      console.error(e);
    } finally {
      isLoading.value = false; // Stop loading
    }
  }
  async function getBooksAll() {
    isLoading.value = true; // Start loading
    try {
      const res = await bookService.getBooksAll();
      booksAllAll.value = res.data;

      if (books.value.length > 0) {
        bookAll.value = booksAllAll.value[0];
        ListBookAll.value = booksAllAll.value[0];
      }
    } catch (e) {
      errorMessage.value = "Failed to load books.";
      console.error(e);
    } finally {
      isLoading.value = false; // Stop loading
    }
  }
  async function getBookById(id: number) {
    isLoading.value = true; // Start loading
    try {
      const res = await bookService.getBookById(id);
      book.value = res.data;
    } catch (e) {
      errorMessage.value = `Failed to load book with ID: ${id}`;
      console.error(e);
    } finally {
      isLoading.value = false; // Stop loading
    }
  }

  async function getBooksByCategory(id: number) {
    isLoading.value = true; // Start loading
    
    try {
      const res = await bookService.getBooksByCategory(id);
      console.log("Fetched books:", books);
      console.log("API Response:", res); // Log the API response
      bookByCate.value = res.data; // Update the reactive property with the data
      return res.data;
      
    } catch (e) {
      errorMessage.value = `Failed to load books for category ID: ${id}`;
      console.error(e);
      return null; // Return null in case of error
    } finally {
      isLoading.value = false; // Stop loading
    }
  }
  

  return {
    book,
    books,
    bookByCate,
    getBooks,
    getBooksByCategory,
    ListBook,
    editedBook,
    getBookById,
    isLoading,
    errorMessage,
    keepBookId,
    bookAll,
    getBooksAll,
    ListBookAll,
    booksAllAll
  };
});
