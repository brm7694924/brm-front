import { defineStore } from 'pinia';
import { ref } from 'vue';
import type Feed from '@/types/Feed';
import feedService from '@/services/feedService'; // นำเข้า feedService

export const useFeedStore = defineStore('feedStore', () => {
  const feeds = ref<Feed[]>([]);
  const isLoading = ref<boolean>(false);
  const error = ref<string | null>(null);

  // ฟังก์ชันสำหรับโหลด feeds
  const loadFeeds = async () => {
    isLoading.value = true;
    error.value = null;
    try {
      const response = await feedService.getFeeds(); // เรียกใช้ getFeeds
      feeds.value = response.data; // เก็บข้อมูลลงใน feeds
    } catch (err: any) {
      error.value = 'Failed to load feeds';
    } finally {
      isLoading.value = false;
    }
  };

  async function getFeeds() {
    try {
      const res = await feedService.getFeeds()
      feeds.value = res.data
    } catch (e) {
      console.log(e)
    }
  }

  const getFeedByUserId = async (userId: number) => {
    isLoading.value = true;
    error.value = null;
    try {
      const response = await feedService.getFeedsByUserId(userId); // Call the service with user ID
      feeds.value = response.data; // Store user-specific feeds
    } catch (err: any) {
      error.value = 'Failed to load feeds for user';
    } finally {
      isLoading.value = false;
    }
  };
  return {
    feeds,
    isLoading,
    error,
    loadFeeds,
    getFeedByUserId,
    getFeeds
  };
});
