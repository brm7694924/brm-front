import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import router from "@/router";
import { useUsersStore } from "./user";
import { useMessageStore } from "./messagestore";
import type User from "@/types/User";

export const useAuthStore = defineStore("auth", () => {
  const userStore = useUsersStore();
  const messageStore = useMessageStore();
  const loginEmail = ref("");

  // ฟังก์ชันตรวจสอบการล็อกอิน
  const isLogin = computed(() => {
    // ตรวจสอบว่า loginEmail ไม่ว่างและมี token ใน localStorage
    return loginEmail.value !== "" && localStorage.getItem("token") !== null;
  });

  // ฟังก์ชัน login
  const login = async (username: string, password: string): Promise<void> => {
    try {
      // ตรวจสอบว่ามีค่า username และ password ที่ถูกต้อง
      if (!username || !password) {
        throw new Error("Email หรือ Password ไม่สามารถเป็นค่าว่างได้");
      }

      console.log("Logging in with:", { username, password });

      // ส่งคำขอไปยัง backend
      const res = await auth.login(username, password);

      console.log("Response from backend:", res);

      // เก็บ token และ user ลง localStorage ถ้าล็อกอินสำเร็จ
      if (res.status === 201) {
        localStorage.setItem("user", JSON.stringify(res.data.user));
        localStorage.setItem("token", res.data.access_token);
        loginEmail.value = res.data.user.email; // อัปเดตค่า loginEmail

        console.log("Login successful, user data stored in local storage.");

        // นำทางไปยังหน้าหลักหลังล็อกอินสำเร็จ
        router.push('/'); // ปรับเปลี่ยนเป็นเส้นทางที่ต้องการ
      }
    } catch (e) {
      console.error("Error during login:", e);
      messageStore.showError("Email หรือ Password ไม่ถูกต้อง");
    }
  };

  const logout = (): void => {
    loginEmail.value = ""; // รีเซ็ต loginEmail
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.push("/");
  };

  const register = async (
    user_password: string,
    user_email: string,
    user_name: string,
  ) => {
    try {
      const response = await auth.authenticate(user_password, user_email, user_name);
      console.log(response);
      if (response.data != null) {
        const user: User = {
          user_id: response.data.id,
          user_name: response.data.name,
          user_email: response.data.email,
          user_password: response.data.password,
          role: response.data.role,
        };
        userStore.setUser(user);
        router.push("/");
      } else {
        console.error("User does not have customer");
        return null;
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString);
    loginEmail.value = user.email; // อัปเดต loginEmail เมื่อดึง user
    return user;
  };

  return { login, logout, register, getUser, isLogin };
});