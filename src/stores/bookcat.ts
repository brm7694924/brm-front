
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import categoryService from '@/services/category'
import type BookCat from '@/types/BookCat'

export const useCategoryStore = defineStore('category', () => {
  const bookcat = ref<BookCat[]>([])
  // const categories = ref([]);
  async function getCategories() {
    try {
      const res = await categoryService.getCategories()
      bookcat.value = res.data
      console.log(bookcat.value)
    } catch (e) {
      console.log(e)
    }
  }

  return { getCategories, categories: bookcat }
})
