
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import bookAuthorService from '@/services/bookaut'
import type BookAut from '@/types/BookAut'

export const useBookAuthorStore = defineStore('BookAut', () => {
  const bookaut = ref<BookAut[]>([])
  async function getBookAut() {
    try {
      const res = await bookAuthorService.getBookAut()
      bookaut.value = res.data
      console.log(bookaut.value)
    } catch (e) {
      console.log(e)
    }
  }

  return { getBookAut, BookAut: bookaut }
})
