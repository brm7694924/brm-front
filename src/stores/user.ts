import { ref } from 'vue';
import { defineStore } from 'pinia';
import userService from '@/services/users'; 
import type User from '@/types/User';
import { useLoadingStore } from './loadingstore';
import router from '@/router';
import { useMessageStore } from './messagestore';

export const useUsersStore = defineStore('user', () => {
  // State
  const user = ref<User[]>([]); // List of users
  const users = ref<User>({
    user_id: 0, 
    user_email: "", 
    user_name: "", 
    user_password: "", 
    user_img: ""
  }); // Single user object
  const editedUser = ref<User>({
    user_email: "",
    user_name: "",
    user_password: "",
    user_img: ""
  }); // For editing user data

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  // Fetch all users
  async function getUsers() {
    try {
      const res = await userService.getUsers();
      console.log(res);
      user.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  // Fetch user by ID
  async function getUserById(user_id: number) {
    try {
      const res = await userService.getUserById(user_id); // Fetch user by ID
      users.value = res.data; // Store the specific user data
    } catch (error) {
      console.error('Error fetching user data:', error);
    }
  }

  // Set user data (for editing purposes)
  function setUser(user: User) {
    editedUser.value = { ...user }; // Spread the user data into editedUser
    localStorage.setItem('user', JSON.stringify(user)); // Store user in local storage
  }

  // Get user data from local storage
  function getUser() {
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      editedUser.value = JSON.parse(storedUser); // Parse stored user data
    }
    return editedUser.value;
  }

  // Save or update user data
  async function saveUser() {
    loadingStore.isLoading = true; // Show loading
    try {
      let res;
      if (editedUser.value?.user_id) {
        // Update existing user
        res = await userService.updateUser(editedUser.value.user_id, editedUser.value);
      } else {
        // Save new user
        res = await userService.saveUser(editedUser.value);
        router.replace("/login"); // Redirect after saving new user
      }
    } catch (e) {
      console.error(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล User ได้"); // Show error message
    } finally {
      loadingStore.isLoading = false; // Hide loading
    }
  }

  // Login function
  const login = (loginEmail: string, password: string): boolean => {
    const index = user.value.findIndex((item) => item.user_name === loginEmail);
    if (index >= 0) {
      const userlog = user.value[index];
      if (userlog.user_password === password) {
        // Successful login
        return true;
      }
      return false;
    }
    return false;
  }

  // Handle file uploads
  const handleFileUpload = (event: Event) => {
    const files = (event.target as HTMLInputElement).files;
    if (files) {
      editedUser.value.files = Array.from(files);  // Dynamically add files
    }
  };

  return {
    user,
    users,
    editedUser,
    getUserById,
    getUsers,
    setUser,
    getUser,
    saveUser,
    login,
    handleFileUpload
  };
});
