import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import reviewsService from '@/services/reviews'
import type Review from '@/types/Review'
import type Book from '@/types/Books'
import { useLoadingStore } from './loadingstore'

export const useReviewStore = defineStore('review', () => {
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const reviews = ref<Review[]>([])
  const reviewsByBookId = ref<Review[]>([])
  const editedRew = ref<Review>({ rev_star: 0, rev_comment: "" });
  const reviewList = ref<
    {
      book: Book
      book_title?: string
      book_content?: string
      book_score?: number
      book_pros?: string
      book_cons?: string
      book_img?: string
    }[]
  >([])

  async function getReviews(user_id?: number) {
    try {
      let res;
      if (user_id) {
        // Fetch reviews filtered by user_id
        res = await reviewsService.getReviewsByUserId(user_id);
      } else {
        // Fetch all reviews if no user_id is provided
        res = await reviewsService.getReviews();
      }
      reviews.value = res.data || []; // Store the fetched reviews or empty array if no data
    } catch (e) {
      console.error('Error fetching reviews:', e)
    }
  }


  async function getReviewsByBookId(bookId: number) {
    try {
      const res = await reviewsService.getReviewsByBookId(bookId)
      reviewsByBookId.value = res.data
    } catch (e) {
      console.log(e)
    }
  }

  async function addReview(reviewData: Review) {
    try {
      const res = await reviewsService.createReview(reviewData)
      reviews.value.push(res.data) // อัปเดตรีวิวในลิสต์ที่มีอยู่
    } catch (e) {
      console.error('ไม่สามารถเพิ่มรีวิวได้:', e)
      throw e // ส่งข้อผิดพลาดกลับเพื่อจัดการในคอมโพเนนต์
    }
  }

  async function deleteReview(id: number) {
    try {
      const res = await reviewsService.deleteReview(id); // Call the delete API
      reviews.value = reviews.value.filter(review => review.rev_id !== id); // Remove the deleted review from the state
      await getReviews();
      console.log(`Review with ID ${id} deleted successfully`); // Log success message
    } catch (e) {
      console.error('Error deleting review:', e); // Handle error
    }
    loadingStore.isLoading = false;
  }
  


  async function updateReview(id: number, reviewData: Review) {
    try {
      await reviewsService.updateReview(id, reviewData)
      const index = reviews.value.findIndex(review => review.rev_id === id)
      if (index !== -1) {
        reviews.value[index] = { ...reviews.value[index], ...reviewData }
      }
    } catch (e) {
      console.error('Error updating review:', e)
    }
  }


  // Function to get reviews by user ID
  async function getReviewsByUserId(userId: number) {
    try {
      const res = await reviewsService.getReviewsByUserId(userId);
      console.log('Fetched reviews:', res.data); // ดูข้อมูลที่ได้จาก API
      reviews.value = res.data; // อัปเดตรีวิวทั้งหมดในลิสต์
    } catch (e) {
      console.log('Error fetching reviews:', e); // ตรวจสอบข้อผิดพลาด
    }
  }
  
  // watch(dialog, (newDialog, oldDialog) => {
  //   console.log(newDialog);
  //   if (!newDialog) {
  //     editedRew.value = { rev_star: 0, rev_comment: "" };
  //   }
  // });
  
  function editReview(reviewData: Review) {
    editedRew.value = JSON.parse(JSON.stringify(reviewData));
  }
  return { 
    getReviews, 
    reviews, 
    reviewList, 
    getReviewsByBookId, 
    reviewsByBookId,
    addReview,
    updateReview,
    getReviewsByUserId ,
    deleteReview,
    editReview,
    editedRew    
  }
})


// const reviews = [
//     {
//       storeName: "Coffee Paradise",
//       rating: 5,
//       reviewText: "The best coffee shop in town! The atmosphere is great and the coffee is amazing.",s
//     },
//     {
//       storeName: "Book Haven",
//       rating: 4,
//       reviewText: "A lovely place with a wide selection of books. The staff is very friendly.",
//     },
//     {
//       storeName: "Gadget World",
//       rating: 3,
//       reviewText: "Good selection of gadgets but the prices are a bit high.",
//     },
//     // more reviews can be added here
//   ];

//   export default reviews;

