import http from "./axios"

function getCategories() {
    return http.get("/categories");
  }


  function getBookCatById(id: number) {
    return http.get(`/bookcats/${id}`); // ดึงข้อมูลเฉพาะด้วย ID
  }
 


  export default {getCategories};