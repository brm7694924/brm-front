import http from './axios'
function getBooks() {
  return http.get('/books')
}
function getBooksAll() {
  return http.get('/books/all')
}

function getBookById(id:any) {
  return http.get(`/books/${id}`)
}


function getBookImgById(id:number) {
  return http.get(`/books/${id}/image`)
}


function getBooksByCategory(id:number) {
  return http.get(`/books/category/${id}`);
  
}


export default { getBooks,getBookById, getBooksByCategory,getBookImgById,getBooksAll  };
