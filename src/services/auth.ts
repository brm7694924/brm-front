import http from "./axios";

function login(username: string, password: string) {
  // ส่งคำขอไปยัง backend ด้วยข้อมูล username และ password โดยตรง
  return http.post("/auth/login", { 
    username: username, // ฟิลด์นี้ต้องตรงกับ backend
    password: password  // ฟิลด์นี้ต้องตรงกับ backend
  });
}

const authenticate = async (password_: string, username_: string, email_: string) => {
  console.log(JSON.stringify({
    user_name: username_,
    user_password: password_,
    user_email: email_,
    role_id: 1
  }));

  // ส่งคำขอสำหรับการลงทะเบียนไปยัง backend
    return await http.post("/auth/register", {
    user_name: username_, // ฟิลด์นี้ต้องตรงกับ backend
    user_password: password_, // ฟิลด์นี้ต้องตรงกับ backend
    user_email: email_, // ฟิลด์นี้ต้องตรงกับ backend
    role_id: 1
  });
};

export default { login, authenticate };
