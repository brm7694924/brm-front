import http from "./axios"

function getBookCat() {
    return http.get("/bookcat");
  }
  function getBookCatById(id: number) {
    return http.get(`/bookcats/${id}`); // ดึงข้อมูลเฉพาะด้วย ID
  }
  export default {getBookCat,getBookCatById};