import http from './axios'
import type User from "@/types/User";
function getUsers() {
  return http.get('/users')
}

function getUserById(id:any) {
  return http.get(`/users/${id}`)
}
function saveUser(User: any ) {
  return http.get(`/Users`, User);
}

  function updateUserImg(id: number, files:File[]) {
    const formDate = new FormData();
    formDate.append("file",files[0])
    return http.patch(`/Users/${id}/image`, formDate,{headers: {
      "Content-Type" : "multipart/form-data"
    }});
  }
  function updateUser(id: number, User: User) {
    return http.patch(`/Users/${id}`, User);
  }
  function deleteUser(id: number) {
    return http.delete(`/Users/${id}`);
  }

export default { getUsers,getUserById,saveUser,updateUserImg,updateUser, deleteUser};
