// src/services/feedService.ts
import axios, { type AxiosResponse } from 'axios';
import type Feed from '@/types/Feed';
import http from './axios';

// Function to fetch feeds
export const getFeeds = () => {
  return http.get<Feed[]>('/feeds'); // Fetch feeds
};

// Function to create a feed
const createFeed = async (feedsData: FormData): Promise<AxiosResponse<Feed>> => {
  return await http.post('/feeds', feedsData); // Use http to post FormData
};

function getFeedsByUserId(userId: number) {
  return http.get(`/feeds/user/${userId}`); // เรียก API ที่ดึงรีวิวตาม user_id
}

// Export functions
export default { getFeeds, createFeed, getFeedsByUserId };
