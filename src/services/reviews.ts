import axios, { type AxiosResponse } from 'axios';
import http from './axios'
import type Review from '@/types/Review';
function getReviews() {
  return http.get("/reviews");
}

async function getReviewsByUserId(userId: number) {
  return await http.get(`/reviews/user/${userId}`); // Adjust the endpoint accordingly
}

function getReviewsByBookId(bookId: number) {
  return http.get(`/reviews/book/${bookId}`); // เรียก API ที่ดึงรีวิวตาม book_id
}

const createReview = async (reviewData: any): Promise<AxiosResponse<any>> => {
  return await http.post('/reviews', reviewData); // ใช้ http แทน axios
}


function updateReview(id: number, Review: Review) {
  return http.patch(`/reviews/${id}`, Review);
}
function deleteReview(id: number) {
  return http.delete(`/reviews/${id}`);
}
export default {getReviewsByUserId,getReviews, getReviewsByBookId,createReview,updateReview,deleteReview};

