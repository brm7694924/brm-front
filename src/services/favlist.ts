import http from './axios'
function getFavlist() {
  return http.get('/myfavorites')
}
function getFavlistById(id: any) {
  return http.get(`/myfavorites/${id}`)
}

function getFavlistByUserId(id: any) {
  return http.get(`/myfavorites/userId/books/${id}`)
}
function addNewMyfavoritesBook(userId: number, bookId: Number) {
  return http.post('/myfavorites', {
    user_id: userId, // Key matches the expected API field name
    book_id: bookId // Key matches the expected API field name
  })
}
export default { getFavlist, getFavlistById, getFavlistByUserId, addNewMyfavoritesBook }
