import http from "./axios"

function getBookAut() {
    return http.get("/book_author");
}


function getBookAuthor(id:any) {
    return http.get(`/author/${id}`);
}

  export default {getBookAut};