import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import ReportPage from '@/components/ReportPage.vue'
import ReportDe from '@/components/ReportDe.vue'
import ReportConfirm from '@/dialogs/ReportConfirm.vue'
import ReportCancel from '@/dialogs/ReportCancel.vue'
import ReviewPage from '@/components/ReviewPage.vue'
import ReviewReportConfirm from '@/dialogs/ReviewReportConfirm.vue'
import ReviewHistory from '@/components/ReviewHistory.vue'
import ReviewDeleteConfirm from '@/dialogs/ReviewDeleteConfirm.vue'
import ReviewEdit from '@/components/ReviewEdit.vue'
import UserManageDel from '@/components/UserManageDel.vue'
import UserManage from '@/components/UserManage.vue'
import UserDelConfirm from '@/dialogs/UserDelConfirm.vue'
import FavListView from '@/views/FavListView.vue'
import MainHeader from '@/components/Header/MainHeader.vue'
import FeedPage from '@/components/FeedPage.vue'
import FeedHistory from '@/components/FeedHistory.vue'
import FeedEdit from '@/components/FeedEdit.vue'
import EditProfile from '@/components/EditProfile.vue'
import BookMangement from '@/components/BookMangement.vue'
import BookDelConfirm from '@/dialogs/BookDelConfirm.vue'
import UserProfile from '@/components/UserProfile.vue'
import LoginForm from '@/components/LoginForm.vue'
import RegisterForm  from '@/components/RegisterForm.vue'
import FeedDeleteConfirm from '@/dialogs/FeedDeleteConfirm.vue'
import UserAccountDel from '@/dialogs/UserAccountDel.vue'
import AddFeed from '@/components/AddFeed.vue'
import testView from '../views/testView.vue'
import FrameworkBooks from '@/components/FrameworkBooks.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',    
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: AboutView
    },
  
    {
      path: '/reports',
      name: 'reports',
      component: ReportPage
    },
    {
      path: '/reportCon',
      name: 'reportCon',
      component: ReportConfirm
    },
    {
      path: '/reportCan',
      name: 'reportCan',
      component: ReportCancel
    },
    {
      path: '/reportDe',
      name: 'reportDe',
      component: ReportDe
    },
    {
      path: '/review/:id',
      name: 'review',
      component: ReviewPage
    },
    {
      path: '/reviewReportCon',
      name: 'reviewReportCon',
      component: ReviewReportConfirm
    },
    {
      path: '/reviewHistory',
      name: 'reviewHistory',
      component: ReviewHistory
    },
    {
      path: '/reviewDeleteConfirm',
      name: 'reviewDeleteConfirm',
      component: ReviewDeleteConfirm
    },
    {
      path: '/reviewEdit',
      name: 'reviewEdit',
      component: ReviewEdit
    },
    {
      path: '/userManage',
      name: 'userManage',
      component: UserManage
    },
    // {
    //   path: '/userManageDel',
    //   name: 'userManageDel',
    //   component: UserManageDel
    // },
    {
      path: '/userDelConfirm',
      name: 'userDelConfirm',
      component: UserDelConfirm
    },
    {
      path: '/accountDel',
      name: 'accountDel',
      component: UserAccountDel
    },
    {
      path: '/feed',
      name: 'feed',
      component: FeedPage
    },
    {
      path: '/feedHistory',
      name: 'feedHistory',
      component: FeedHistory
    },
    {
      path: '/feedEdit',
      name: 'feedEdit',
      component: FeedEdit
    },
    {
      path: '/feedDeleteConfirm',
      name: 'feedDeleteConfirm',
      component: FeedDeleteConfirm
    },
    {
      path: '/editPro',
      name: 'EditProfile',
      component: EditProfile
    },
    {
      path: '/bookmangement',
      name: 'bookmangement',
      component: BookMangement
    },
    {

      path: '/bookDelConfirm',
      name: 'bookDelConfirm',
      component: BookDelConfirm

    },
    {

      path: '/profile',
      name: 'profile',
      component: UserProfile

    },
    {

      path: '/login',
      name: 'login',
      component: LoginForm

    },
    {

      path: '/register',
      name: 'register',
      component: RegisterForm

    },
    
    {

      path: '/addfeed',
      name: 'addfeed',
      component: AddFeed

    },
    
    {

      path: '/frameworkBooks',
      name: 'frameworkBooks',
      component: FrameworkBooks

    },
    {

      path: '/test',
      name: 'testView',
      component: testView

    },

    {
      path: '/favList',
      name: 'favoliteList',
      components: {
        default: FavListView,
        header: MainHeader
    },

    },
  
  ]
  });

export default router
